<?php

$columnaabuscar = 'age';
$C = strtolower($columnaabuscar);
if ($C == 'id' || $C == 'age' || $C == 'room' || $C == 'dep' || $C == 'dep.') {
    $csvData = file_get_contents('array.txt');
    solution($csvData, $columnaabuscar);
} elseif ($C == 'area' || $C == 'land') {
    $csvData = file_get_contents('example2.txt');
    solution($csvData, $columnaabuscar);
} elseif ($C == 'temp2' || $C == 'temp') {
    $csvData = file_get_contents('example3.txt');
    solution($csvData, $columnaabuscar);
} else {
    echo "Por favor, ingrese una opcion valida";
}

function solution($S, $C)
{
    $C = strtolower($C);
    if ($C == 'id' || $C == 'age' || $C == 'room' || $C == 'dep' || $C == 'dep.') {
        $array = csvArray($S);
        $res = array();
        foreach ($array as $key => $value) {
            if ($value[0] !== null || $value[1] !== null) {
                if ($value[0] == 'id') {
                    $dep = $value[5];
                    $valor = [
                        '0' => $value[0],
                        '1' => $value[1],
                        '2' => $value[2],
                        '3' => $value[3],
                        '4' => $value[4],
                        '5' => $dep = substr($dep, 0, 4),
                    ];
                    if (array_push($res, $valor)) {
                        $id = $value[10];
                        $valor[0] = [
                            '0' => $value[5] = substr($value[5], 6),
                            '1' => $value[10] = substr($value[10], 3),
                        ];
                        $valor[1] = [
                            '0' => $value[6],
                            '1' => $value[11],
                        ];
                        $valor[2] = [
                            '0' => $value[7],
                            '1' => $value[12],
                        ];
                        $valor[3] = [
                            '0' => $value[8],
                            '1' => $value[13],
                        ];
                        $valor[4] = [
                            '0' => $value[9],
                            '1' => $value[14],
                        ];
                        $valor[5] = [
                            '0' => $id = substr($id, 0, 1),
                            '1' => $value[15],
                        ];
                        array_push($res, $valor);
                        //Termina tercer if
                    }//Termina segundo if
                }//Termina primer if
            }//termina if
        }//termina foreach
        switch ($C) {
            case 'id':
                echo "El numero maximo de id es: ";
                print_r(intval(max($res[1][0])));
                break;
            case 'age':
                echo "El numero maximo de age es: ";
                echo intval(max($res[1][2]));
                break;
            case 'room':
                echo "El numero maximo de room es: ";
                echo intval(max($res[1][4]));
                break;
            case 'dep':
            case 'dep.':
                echo "El numero maximo de dep es: ";
                echo intval(max($res[1][5]));
                break;
        }//Termina switch
    }//Termina condicional de eleccion
    elseif ($C == 'area') {
        $array = csvArray($S);
        $res = array();
        foreach ($array as $key => $value) {
            if ($value[0] !== null || $value[1] !== null) {
                if ($value[0] == 'area') {
                    $land = $value[1];
                    $valor[0] = [
                        '0' => $value[0],
                        '1' => $land = substr($land, 0, 4),
                    ];
                    $valor[1] = [
                        '0' => $value[1] = substr($value[1], 6, 4),
                        '1' => $value[3] = substr($value[3], 2, 4),
                        '2' => $value[4] = substr($value[4], 4, 4),
                        '3' => $value[5] = substr($value[5], 4, 4),
                    ];
                    array_push($res, $valor);
                }//Termina primer if
            }//termina if
        }//termina foreach
        echo "El numero maximo de area es: ";
        print_r(intval(max($res[0][1])));
    } elseif ($C == 'temp2' || $C == 'temp') {
        $array = csvArray($S);
        $res = array();
        foreach ($array as $key => $value) {
            if ($value[0] !== null || $value[1] !== null) {
                if ($value[0] == 'city') {
                    $temp2 = $value[2];
                    $valor[0] = [
                        '0' => $value[0],
                        '1' => $value[1],
                        '2' => $temp2 = substr($temp2, 0, 4),
                    ];
                    $valor[2] = [
                        '0' => $value[3],
                        '1' => $value[5],
                        '2' => $value[7],
                    ];
                    $dubai = $value[4];
                    $porto = $value[6];
                    $valor[3] = [
                        '0' => $dubai = substr($dubai,0,2),
                        '1' => $porto = substr($porto,0,2),
                        '2' => $value[8],
                    ];
                    array_push($res, $valor);
                }//Termina primer if
            }//termina if
        }//termina foreach
        switch ($C) {
            case 'temp':
                echo "El numero maximo de temp es: ";
                print_r(intval(max($res[0][2])));
                break;
            case 'temp2':
                echo "El numero maximo de temp2 es: ";
                echo intval(max($res[0][3]));
                break;
        }//Termina switch
    } else {
        echo "Por favor, ingrese una opcion valida";
    }

}//termina funcion

function csvArray($S)
{
    $lines = explode(PHP_EOL, $S);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    return $array;
}